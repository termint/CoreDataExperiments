//
//  ProductDescription.swift
//  CoreDataExperiments
//
//  Created by Alexey Golovenkov on 22.04.2023.
//

import CoreData
import Foundation

protocol CoreEntity: NSManagedObject {
    
    static func entityDescription() -> NSEntityDescription
}

extension CoreEntity {
    
    static func entityDescription() -> NSEntityDescription {
        let entityName = String(describing: self)
        let properties = propertyList()
        let entity = NSEntityDescription()
        entity.name = entityName
        entity.managedObjectClassName = NSStringFromClass(self)
        entity.properties = properties
        return entity
    }
    
    static func propertyList() -> [NSPropertyDescription] {
        let attributes = PropertyParser.parse(self)
        
        let descriptions = attributes.compactMap { (attribute) -> NSPropertyDescription? in
            guard attribute.isDynamic else {
                return nil
            }
            let propertyName = attribute.name
            let attributeType = attribute.attributeType
            if attributeType != .undefinedAttributeType {
                let coreDataAttribute = NSAttributeDescription()
                coreDataAttribute.name = propertyName
                coreDataAttribute.attributeType = attributeType
                coreDataAttribute.defaultValue = nil
                return coreDataAttribute
            }
            return nil
        }
        return descriptions
    }
}
