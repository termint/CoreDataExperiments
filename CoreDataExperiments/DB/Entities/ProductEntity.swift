//
//  ProductEntity.swift
//  CoreDataExperiments
//
//  Created by Alexey Golovenkov on 22.04.2023.
//

import Foundation
import CoreData

@objc
class ProductEntity: NSManagedObject, CoreEntity {
    
    @NSManaged var name: String?
    @NSManaged var like: Bool
}
