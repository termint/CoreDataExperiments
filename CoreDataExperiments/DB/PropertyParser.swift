//
//  PropertyParser.swift
//  CoreDataExperiments
//
//  Created by Alexey Golovenkov on 22.04.2023.
//

import Foundation
import CoreData

public struct PropertyParser {
    
    public struct Attributes {
        let name: String
        let propertyType: String
        let isReadOnly: Bool
        let isDynamic: Bool
        let isCopy: Bool
        let isNonatomic: Bool
        
        var attributeType: NSAttributeType {
            switch propertyType {
            case "s":
                return .integer16AttributeType
            case "i":
                return .integer32AttributeType
            case "q":
                return .integer64AttributeType
            case "@\"NSDecimalNumber\"":
                return .decimalAttributeType
            case "d":
                return .doubleAttributeType
            case "f":
                return .floatAttributeType
            case "@\"NSString\"":
                return .stringAttributeType
            case "B":
                return .booleanAttributeType
            case "@\"NSDate\"":
                return .dateAttributeType
            case "@\"NSData\"":
                return .binaryDataAttributeType
            case "@\"NSUUID\"":
                return .UUIDAttributeType
            case "@\"NSURL\"":
                return .URIAttributeType
            default:
                return .undefinedAttributeType
            }
        }
    }

    public static func parse(_ source: AnyClass) -> [Attributes] {
        var count = UInt32()
        guard let properties: UnsafeMutablePointer<objc_property_t> = class_copyPropertyList(source, &count) else {
            return []
        }
        var propertyNames = [Attributes]()
        let intCount = Int(count)
        for index in 0..<intCount {
            let property: objc_property_t = properties[index]
            guard let propertyName = NSString(utf8String: property_getName(property)) as? String else {
                print("Couldn't unwrap property name for \(property)")
                break
            }
            guard let attrString = NSString(utf8String: property_getAttributes(property)!) as? String else {
                print("Couldn't unwrap property attrs for \(property)")
                break
            }
            var attrsArr = attrString.components(separatedBy: ",")
            var firstAttr = attrsArr.removeFirst()
            let firstChar = firstAttr.removeFirst()
            guard firstChar == "T" else {
                print("Couldn't unwrap attribute type for \(propertyName) as \(attrString)")
                break
            }
            let isReadOnly = attrsArr.contains("R")
            let isDynamic = attrsArr.contains("D")
            let isCopy = attrsArr.contains("C")
            let isNonatomic = attrsArr.contains("N")
            
            let attribute = Attributes(
                name: propertyName,
                propertyType: firstAttr,
                isReadOnly: isReadOnly,
                isDynamic: isDynamic,
                isCopy: isCopy,
                isNonatomic: isNonatomic
            )
            
            propertyNames.append(attribute)
        }

        free(properties)
        return propertyNames
    }
    
}
