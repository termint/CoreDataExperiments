//
//  CoreDataModel.swift
//  CoreDataExperiments
//
//  Created by Alexey Golovenkov on 22.04.2023.
//

import Foundation
import CoreData

class CoreDataModel {
    
    static private(set) var descriptions: [NSEntityDescription] = []
    
    static func register(_ entity: NSEntityDescription) {
        descriptions.append(entity)
    }
    
    static func makeModel() -> NSManagedObjectModel {
        let model = NSManagedObjectModel()
        model.entities = descriptions
        return model
    }    
}
