//
//  ViewController.swift
//  CoreDataExperiments
//
//  Created by Alexey Golovenkov on 22.04.2023.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var slider: UISlider!
    
    let frc: NSFetchedResultsController = {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let request = NSFetchRequest<ProductEntity>(entityName: "ProductEntity")
        request.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
        let controller = NSFetchedResultsController(fetchRequest: request, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        try? controller.performFetch()
        return controller
    } ()
    
    var context: NSManagedObjectContext {
        (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    }
    
    var cellsCount: Int {
        frc.sections?[0].numberOfObjects ?? 0
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tableView.dataSource = self
        frc.delegate = self
    }

    @IBAction func stepperValueChanged(_ sender: UIStepper) {
        if Int(sender.value) > cellsCount {
            let product = ProductEntity(context: context)
            product.name = "Product #\(tableView(tableView, numberOfRowsInSection: 0))"
            product.like = false
        } else {
            let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
            let request = NSFetchRequest<ProductEntity>(entityName: "ProductEntity")
            request.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
            guard let product = try? context.fetch(request).last else {
                return
            }
            context.delete(product)
        }
        slider.maximumValue = Float(sender.value)
    }
    
    @IBAction func sliderValueChanged(_ sender: UISlider) {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let request = NSFetchRequest<ProductEntity>(entityName: "ProductEntity")
        request.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
        guard let products = try? context.fetch(request) else {
            return
        }
        let value = Int(sender.value)
        for (index, product) in products.enumerated() {
            product.like = index <= value
        }
    }
    
    @IBAction func saveButtonPressed() {
        try? context.save()
    }
    
    @IBAction func rollbackButtonPressed() {
        context.rollback()
    }
    
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let cells = frc.sections?[section].numberOfObjects ?? 0
        return cells
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let product = frc.object(at: indexPath)
        cell.textLabel?.text = product.name
        cell.accessoryType = product.like ? .checkmark : .none
        return cell
    }
}

extension ViewController: NSFetchedResultsControllerDelegate {
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .fade)
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .fade)
        case .update:
            tableView.reloadRows(at: [indexPath!], with: .none)
            break
        case .move:
            break
        @unknown default:
            break
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
}
